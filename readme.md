RPS Logger
==========

RPS Logger class registers and sends the descriptions of the events to a remote server for further logging.

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

### Pre-requisites
> Note: Check the [composer.json](https://github.com/kartik-v/yii2-dropdown-x/blob/master/composer.json) for this extension's requirements and dependencies. 
You must set the `minimum-stability` to `dev` in the **composer.json** file in your application root folder before installation of this extension OR
if your `minimum-stability` is set to any other value other than `dev`, then set the following in the require section of your composer.json file

### Install

Either run

```
$ php composer.phar require solutions/logger "@dev"
```

or add

```
"solutions/logger": "@dev"
```

to the ```require``` section of your `composer.json` file.

## Usage
```php
 
use Logger\Logger;
     
$endPoint = 'http://test-endpoint.com';

$site = 'my-site';
$time = time();
$event = Logger::CREATE;
$user = 'John Doe';
$text = 'Added a test record to the database';

$logger = new Logger($endPoint);
$result = $logger->add([
    'site' => $site,
    'time' => $time,
    'event' => $event,
    'user' => $user,
    'text' => $text
]); 

```