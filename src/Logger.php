<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 7/9/15
 * Time: 3:11 PM
 */

namespace Logger;
use Curl\Curl;
use Webmozart\Json\JsonEncoder;

class Logger {

    const CREATE = 'c';
    const UPDATE = 'u';
    const DELETE = 'd';

    private $endPoint;

    /**
     * Should data be encoded
     * @var bool
     */
    public $encode = false;

    public function __construct($endPoint) {
        $this->setEndPoint($endPoint);
        $this->init();
    }

    /**
     * @param array $data. The structure:
     *
     * ~~~
     * [
     *    'site',  // the site name
     *    'time',  // unix operation timestamp
     *    'event', // see the CRUD constants
     *    'user',  // user, who committed the operation
     *    'text',  // the description of the operation
     * ]
     * ~~~
     *
     * @return array $result. The structure:
     *
     * ~~~
     * [
     *     'error_code', // zero on success. On error returns curl or http error code
     *     'error_message', // empty on success. On error returns curl error message or http error message
     * ]
     * ~~~
     *
     * ~~~
     * use Logger\Logger;
     *
     * $endPoint = 'http://test-endpoint.com';
     *
     * $site = 'my-site';
     * $time = time();
     * $event = Logger::CREATE;
     * $user = 'John Doe';
     * $text = 'Added a test record to the database';
     *
     * $logger = new Logger($endPoint);
     * $result = $logger->add([
     *    'domain' => $domain,
     *    'log_time' => $log_time,
     *    'action' => $action,
     *    'user' => $user,
     *    'action' => $action
     * ]);
     * ~~~
     *
     */
    public function add($data) {
        $curl = new Curl();
        $curl->setOpt(CURLOPT_HEADER,0);
        $curl->post($this->getEndPoint(),$this->encode?(new JsonEncoder())->encode($data):$data);

        return ['error_code' => $curl->error_code, 'error_message' => $curl->error_message,
        'response'=>$curl->response];
    }

    public function setEndPoint($endPoint) {
        if ($endPoint === null) {
            throw new \ErrorException('Logger->endPoint was not provided');
        }
        $this->endPoint = $endPoint;
    }

    private function getEndPoint() {
        return $this->endPoint;
    }

    private function init() {

    }
}